package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			// TODO Auto-generated method stub
			return " ".repeat(width - text.length()) + text;
		}
		
		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return text + " ".repeat(width - text.length());

		}


		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			String[] words = text.split("\\W+");
			int extra = (width - text.length()) / (words.length - 1);  
			String a = "";
			for (int i = 0; i < words.length - 1;i++) {
				a += words[i] + " ".repeat(extra);
			}
			a += words[words.length - 1];
			return a;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		
		

	}
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
	}
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
	}
}
