package no.uib.ii.inf112;

public class Testing {

	public static String justify(String text, int width) {
		// TODO Auto-generated method stub
		String[] words = text.split("\\W+");
		int extra = (width - text.length()) / (words.length - 1);  
		String a = "";
		for (int i = 0; i < words.length - 1;i++) {
			a += words[i] + " ".repeat(extra);
		}
		a += words[words.length - 1];
		return a;
	}
	
	public static void main(String[] args) {
		String a = justify("fee fie foo", 15);
		System.out.println(a);
	}
	
}
